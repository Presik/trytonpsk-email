# -*- coding: UTF-8 -*-
# This file is part electronic_mail_template module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.utils import formatdate
from email import encoders
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool
from jinja2 import Template as Jinja2Template
from .mailtrap import MailTrap
from .sendgrid import SendGrid
# from sendgrid import SendGridAPIClient
# from sendgrid.helpers.mail import (
#     Mail, Attachment, FileContent, FileName, FileType, Disposition, ContentId
# )


class Template(ModelSQL, ModelView):
    'Email Template'
    __name__ = 'email.template'
    name = fields.Char('Name', required=True)
    subject = fields.Char('Subject')
    html_body = fields.Text('HTML Body')
    from_email = fields.Char('From Email')
    recipients = fields.Char('Recipients', help='Mails separated by colon')
    attachment_filename = fields.Char('Attachment Filename')
    report = fields.Many2One('ir.action.report', 'Report')
    signature = fields.Boolean('Use Signature', help='The signature from the User details '
        'will be appened to the mail.')
    webapi = fields.Many2One('email.webapi', 'Web API', help='Web API send mail')
    bcc = fields.Char('BCC')
    company = fields.Many2One('company.company', 'Company')

    @classmethod
    def __setup__(cls):
        super(Template, cls).__setup__()
        cls._buttons.update({
            'test_send': {
                'invisible': False,
            }
        })

    @classmethod
    @ModelView.button
    def test_send(cls, records):
        for rec in records:
            rec._test_send()

    def _test_send(self):
        webservice = self.webapi.web_service
        api = self.webapi.api_key
        provider = None
        if webservice == 'sendgrid':
            provider = SendGrid(api)
        elif webservice == 'mailtrap':
            provider = MailTrap(api)
        provider._test_send(self)
        # message = Mail(
        #     from_email=self.from_email,
        #     to_emails='gomezwilson.99@gmail.com',
        #     subject=self.subject,
        #     plain_text_content='Quick and easy to do anywhere, even with Python',
        #     html_content=self.html_body
        # )
        # try:
        #     sg = SendGridAPIClient(self.webapi.api_key)
        #     response = sg.send(message)
        #     print(response.body)
        #     print(response.headers)
        # except Exception as e:
        #     print(e.message)

    # @classmethod
    # def prepare_attachment(cls, data, filename, ext):
    #     if not filename:
    #         filename = 'test_filename'

    #     encoded = base64.b64encode(data).decode()
    #     attachment = Attachment()
    #     attachment.file_content = FileContent(encoded)
    #     _filename = filename + '.' + ext
    #     content_type, _ = mimetypes.guess_type(_filename)
    #     attachment.file_type = FileType(content_type)
    #     attachment.file_name = FileName(_filename)
    #     attachment.disposition = Disposition('attachment')
    #     attachment.content_id = ContentId('Example Content ID')
    #     return attachment

    @classmethod
    def send(cls, template, record, to_recipients=None, attach=False, attachments=[]):
        if not template or (not to_recipients and not template.recipients):
            return
        webservice = template.webapi.web_service
        api = template.webapi.api_key
        provider = None
        if webservice == 'sendgrid':
            provider = SendGrid(api)
        elif webservice == 'mailtrap':
            provider = MailTrap(api)
        return provider.send(template, record, to_recipients, attach, attachments)
        # html_content = cls.engine_jinja2(template.html_body, record, template.webapi.api_response)

        # if not to_recipients:
        #     to_recipients = template.recipients

        # message = Mail(
        #     from_email=template.from_email,
        #     to_emails=to_recipients,
        #     subject=template.subject,
        #     html_content=html_content
        # )
        # if template.report:
        #     ext, data, _, file_name = template.render_report(record)
        #     attachment = cls.prepare_attachment(data, file_name, ext)
        #     message.attachment = attachment

        # """
        # Diccionary list attachments are:
        #     [{
        #         'attachment': <document>,
        #         'file_name': <filenae>,
        #         'extension': <xml, jpg>,
        #     },]
        # """
        # for att in attachments:
        #     attachment = cls.prepare_attachment(
        #         att['attachment'], att['file_name'], att['extension']
        #     )
        #     message.add_attachment(attachment)
        # if template.bcc:
        #     message.add_bcc(template.bcc)

        # try:
        #     sg = SendGridAPIClient(template.webapi.api_key)
        #     response = sg.send(message)
        #     print(response.status_code)
        #     return response
        # except Exception as e:
        #     print(e)

    @classmethod
    def engine_jinja2(cls, expression, record, api_response):
        '''
        :param expression: Expression to evaluate
        :param record: Browse record
        '''
        template_jinja = Jinja2Template(expression)
        return template_jinja.render({'record': record, 'api_response': api_response})

    def render_report(self, record):
        '''Render the report and returns a tuple of data
        '''
        Report = Pool().get(self.report.report_name, type='report')
        report = Report.execute([record.id], {'id': record.id})
        ext, data, filename, file_name = report
        return ext, data, filename, file_name

    def render(self, record):
        '''Renders the template and returns as email object
        :param record: Browse Record of the record on which the template
            is to generate the data on
        :return: 'email.message.Message' instance
        '''
        message = MIMEMultipart()
        message['date'] = formatdate(localtime=1)
        return message

    def render_and_send(self, records):
        """
        Render the template and send
        :param records: List Object of the records
        """
        return None

    # def get_attachments(self, records):
    #     record_ids = [r.id for r in records]
    #     attachments = []
    #     for report in self.reports:
    #         report = Pool().get(report.report_name, type='report')
    #         ext, data, filename, file_name = report.execute(record_ids, {})

    #         if file_name:
    #             filename = self.eval(file_name, record_ids).decode('utf-8')
    #         filename = ext and '%s.%s' % (filename, ext) or filename
    #         content_type, _ = mimetypes.guess_type(filename)
    #         maintype, subtype = (
    #             content_type or 'application/octet-stream'
    #             ).split('/', 1)

    #         attachment = MIMEBase(maintype, subtype)
    #         attachment.set_payload(data)
    #         encoders.encode_base64(attachment)
    #         attachment.add_header(
    #             'Content-Disposition', 'attachment', filename=filename)
    #         attachments.append(attachment)
    #     return attachments
